""" Main Flask module """
from flask import Flask, request
import json
from db import db
from config import Config

application = app = Flask(__name__)
app.config.from_object(Config())


db.init_app(app)



if __name__ == '__main__':
    application.run(host='0.0.0.0', debug=True)
