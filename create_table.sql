CREATE DATABASE `store_db`;
USE `store_db`;
CREATE TABLE `category` (
  `category_id` smallint NOT NULL AUTO_INCREMENT unique,
  `category_name` varchar(25) unique,
  PRIMARY KEY (`category_id`),
  `created_at` timestamp default current_timestamp,
  `deleted_at` datetime default null
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
create table `product`(
  `product_id` smallint NOT NULL AUTO_INCREMENT unique,
  `product_name` varchar(100) unique NOT NULL,
  `category_id` smallint NOT NULL,
  foreign key (`category_id`) references `category`(`category_id`),
  PRIMARY KEY (`product_id`),
  `created_at` timestamp default current_timestamp,
  `deleted_at` datetime default null
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
create table `provider`(
  `provider_id` smallint NOT NULL AUTO_INCREMENT unique,
  `provider_name` varchar(100) NOT NULL unique,
  `provider_email` varchar(255) NOT NULL unique,
  PRIMARY KEY (`provider_id`),
  `created_at` timestamp default current_timestamp,
  `deleted_at` datetime default null
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
create table `purchase_order`(
  `purchase_order_id` smallint NOT NULL AUTO_INCREMENT unique,
  `provider_id` smallint NOT NULL,
  `arrival_date` date NOT NULL,
  `confirmed` tinyint default 0, 
  foreign key (`provider_id`) references `provider`(`provider_id`),
  PRIMARY KEY (`purchase_order_id`),
  `created_at` timestamp default current_timestamp,
  `deleted_at` datetime default null
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
create table `purchase_details`(
  `purchase_details_id` smallint NOT NULL AUTO_INCREMENT unique,
  `purchase_order_id` smallint NOT NULL,
  `product_id` smallint NOT NULL,
  `purchase_product_price` mediumint NOT NULL, 
  `purchase_product_quantity` int NOT NULL, 
  foreign key (`purchase_order_id`) references `purchase_order`(`purchase_order_id`),
  foreign key (`product_id`) references `product`(`product_id`),
  PRIMARY KEY (`purchase_details_id`),
  `created_at` timestamp default current_timestamp,
  `deleted_at` datetime default null
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
alter table  `purchase_details` add constraint check (`purchase_product_price` > 0);
alter table  `purchase_details` add constraint check (`purchase_product_quantity` > 0);